## Concourse siple pipeline for the .Net Core project

login to concourse
```
fly -t tutorial login -c ${concourse_url}
```
execute the task
```
fly -t tutorial execute -c ./build.yml
```
or 
set the pilepline
```
fly -t tutorial set-pipeline -c ./ci/pipeline.yml -p HelloWorldApp
```